
const FIRST_NAME = "Andra-Elisa";
const LAST_NAME = "Ceică";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
    getDetails(){
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee{
    constructor(name, surname, salary){
        super(name, surname, salary);
        this.experience = 'JUNIOR';
    }

    applyBonus()
    {
        if(this.experience === 'JUNIOR') return this.salary+this.salary*0.1;
        if(this.experience === 'MIDDLE') return this.salary+this.salary*0.15;
        if(this.experience === 'SENIOR') return this.salary+this.salary*0.2;
        return this.salary+this.salary*0.1;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

